<?php

// Set up authentication.
/**
 * @var Google_Client $client
 * @var Google_Service_DisplayVideo $service
 */
require 'vendor/autoload.php';
require 'client.php';

var_dump($service->customLists->listCustomLists());
die(PHP_EOL);

// Create a line item object.
$lineItem = new Google_Service_DisplayVideo_LineItem();
$lineItem->setInsertionOrderId('insertion-order-id');
$lineItem->setDisplayName('display-name');
$lineItem->setLineItemType('LINE_ITEM_TYPE_DISPLAY_DEFAULT');
$lineItem->setEntityStatus('ENTITY_STATUS_DRAFT');

// Create and set the line item flight.
$flight = new Google_Service_DisplayVideo_LineItemFlight();
$flight->setFlightDateType('LINE_ITEM_FLIGHT_DATE_TYPE_INHERITED');
$lineItem->setFlight($flight);

// Create and set the line item budget.
$budget = new Google_Service_DisplayVideo_LineItemBudget();
$budget->setBudgetAllocationType(
    'LINE_ITEM_BUDGET_ALLOCATION_TYPE_FIXED'
);
$lineItem->setBudget($budget);

// Create and set the pacing setting.
$pacing = new Google_Service_DisplayVideo_Pacing();
$pacing->setPacingPeriod('PACING_PERIOD_DAILY');
$pacing->setPacingType('PACING_TYPE_EVEN');
$pacing->setDailyMaxMicros(10000);
$lineItem->setPacing($pacing);

// Create and set the frequency cap.
$frequencyCap = new Google_Service_DisplayVideo_FrequencyCap();
$frequencyCap->setMaxImpressions(10);
$frequencyCap->setTimeUnit('TIME_UNIT_DAYS');
$frequencyCap->setTimeUnitCount(1);
$lineItem->setFrequencyCap($frequencyCap);

// Create and set the partner revenue model.
$partnerRevenueModel =
    new Google_Service_DisplayVideo_PartnerRevenueModel();
$partnerRevenueModel->setMarkupType(
    'PARTNER_REVENUE_MODEL_MARKUP_TYPE_CPM'
);
$partnerRevenueModel->setMarkupAmount(10);
$lineItem->setPartnerRevenueModel($partnerRevenueModel);

// Set the list of IDs of the creatives associated with the line item.
lineItem > setCreativeIds('creative-ids');

// Create and set the bidding strategy.
$biddingStrategy = new Google_Service_DisplayVideo_BiddingStrategy();
$fixedBidStrategy = new Google_Service_DisplayVideo_FixedBidStrategy();
$fixedBidStrategy->setBidAmountMicros(100000);
$biddingStrategy->setFixedBid($fixedBidStrategy);
$lineItem->setBidStrategy($biddingStrategy);



// Create the line item.
$result = $this->service->advertisers_lineItems->create(
    'advertiser-id',
    $lineItem
);

printf('Line Item %s was created.\n', $result['name']);