<?php

// Set up authentication.
$client = new Google_Client();
$this->service = new Google_Service_DisplayVideo($client);


// Create an insertion order object.
$insertionOrder = new Google_Service_DisplayVideo_InsertionOrder();
$insertionOrder->setCampaignId('campaign - id');
$insertionOrder->setDisplayName('display - name');
$insertionOrder->setEntityStatus('ENTITY_STATUS_DRAFT');

// Create and add the pacing setting.
$pacing = new Google_Service_DisplayVideo_Pacing();
$pacing->setPacingPeriod('PACING_PERIOD_DAILY');
$pacing->setPacingType('PACING_TYPE_EVEN');
$pacing->setDailyMaxMicros(10000);
$insertionOrder->setPacing($pacing);

// Create and set the frequency cap.
$frequencyCap = new Google_Service_DisplayVideo_FrequencyCap();
$frequencyCap->setMaxImpressions(10);
$frequencyCap->setTimeUnit('TIME_UNIT_DAYS');
$frequencyCap->setTimeUnitCount(1);
$insertionOrder->setFrequencyCap($frequencyCap);

// Create and set the performance goal.
$performanceGoal = new Google_Service_DisplayVideo_PerformanceGoal();
$performanceGoal->setPerformanceGoalType('PERFORMANCE_GOAL_TYPE_CPC');
$performanceGoal->setPerformanceGoalAmountMicros(1000000);
$insertionOrder->setPerformanceGoal($performanceGoal);

// Create a budget object.
$budget = new Google_Service_DisplayVideo_InsertionOrderBudget();
$budget->setBudgetUnit('BUDGET_UNIT_CURRENCY');

// Create a budget segment object.
$budgetSegment =
    new Google_Service_DisplayVideo_InsertionOrderBudgetSegment();
$budgetSegment->setBudgetAmountMicros(100000);

// Create a date range object for the budget segment.
$dateRange = new Google_Service_DisplayVideo_DateRange();

// Create and assign a start date one week from now.
$startDateTime = new DateTime('today + 7 days');
$startDate = new Google_Service_DisplayVideo_Date();
$startDate->setYear($startDateTime->format('Y'));
$startDate->setMonth($startDateTime->format('n'));
$startDate->setDay($startDateTime->format('j'));
$dateRange->setStartDate($startDate);

// Create and assign an end date two weeks from now.
$endDateTime = new DateTime('today + 14 days');
$endDate = new Google_Service_DisplayVideo_Date();
$endDate->setYear($endDateTime->format('Y'));
$endDate->setMonth($endDateTime->format('n'));
$endDate->setDay($endDateTime->format('j'));
$dateRange->setendDate($endDate);

// Assign date range to budget segment.
$budgetSegment->setDateRange($dateRange);

// Set budget segment.
$budget->setBudgetSegments(array($budgetSegment));

// Set budget object.
$insertionOrder->setBudget($budget);

// Call the API, creating the insertion order under the advertiser and
// campaign given.
$result = $this->service->advertisers_insertionOrders->create(
    'advertiser - id',
    $insertionOrder
);

printf('Insertion Order %s was created.\n', $result['name']);

$this
    ->service
    ->advertisers_lineItems_targetingTypes_assignedTargetingOptions
    ->create(
        'advertiser-id',
        'line-item-id',
        'TARGETING_TYPE_BROWSER',
        $assignedTargetingOption
    );

